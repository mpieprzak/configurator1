﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;
using Configurator1.Core.Dtos;
using Configurator1.PresentationModel.ViewModels;

namespace Configurator1.PresentationModel.Presenters
{
    public class EditUsersPresenter : IPresenter<UserDto>
    {
        public object ViewModel { get; private set; }

        public void Show(UserDto userDto)
        {
            ViewModel = new UserViewModel
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName
            };
        }

        public void ShowError(string errorMsg)
        {
            ViewModel = new ErrorOccuranceViewModel
            {
                Message = errorMsg
            };
        }
    }
}