﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;
using Configurator1.Core.Dtos;
using Configurator1.PresentationModel.ViewModels;
using System.Linq;

namespace Configurator1.PresentationModel.Presenters
{
    public class UsersPresenter : IPresenter<UsersDto>
    {
        public object ViewModel { get; private set; }

        public void Show(UsersDto dto)
        {
            ViewModel = ConvertToViewModel(dto);
        }

        public UsersViewModel ConvertToViewModel(UsersDto usersDto)
        {
            return new UsersViewModel
            {
                UserRows = usersDto.UserRows
                    .Select(x => new UserRowViewModel
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName
                    }),

                SelectedUserId = usersDto.SelectedUserId
            };
        }

        public void ShowError(string errorMsg)
        {
            ViewModel = new ErrorOccuranceViewModel
            {
                Message = errorMsg
            };
        }
    }
}
