﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.PresentationModel.ViewModels
{
    public class UsersViewModel
    {
        public IEnumerable<UserRowViewModel> UserRows { get; set; }

        public int? SelectedUserId { get; set; }
    }
}
