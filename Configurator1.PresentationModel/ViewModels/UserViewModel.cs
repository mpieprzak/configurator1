﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.PresentationModel.ViewModels
{
    public class UserViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
