﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.PresentationModel.ViewModels
{
    public class UserRowViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
