﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Configurator1.Core.Interactors;
using Configurator1.Persistance;
using Configurator1.PresentationModel.ViewModels;
using Configurator1.PresentationModel.Presenters;
using Configurator1.Core.Abstract;
using Configurator1.Logger;
using Configurator1.Core.Dtos;

namespace Configurator1.Presentation.Controllers
{
    public class UsersController : Controller
    {
        public IActionResult List()
        {
            MockUserRepository repository = new MockUserRepository();
            //ExceptionThrowingUserRepository repository = new ExceptionThrowingUserRepository();

            UsersPresenter presenter = new UsersPresenter();

            MockLogger logger = new MockLogger();

            GetUsersInteractor interactor = new GetUsersInteractor(presenter, repository, logger);

            interactor.Execute();

            if (presenter.ViewModel is UsersViewModel)
                return View(presenter.ViewModel);
            else //(presenter.ViewModel is ErrorOccuranceViewModel)
                return View("ErrorOccurance", presenter.ViewModel);
            //else return some global error view
        }

        public ViewResult Create()
        {
            return View(new UserViewModel());
        }

        [HttpPost]
        public IActionResult Create(UserViewModel userViewModel)
        {
            MockUserRepository repository = new MockUserRepository();
            //ExceptionThrowingUserRepository repository = new ExceptionThrowingUserRepository();

            UsersPresenter presenter = new UsersPresenter();

            MockLogger logger = new MockLogger();

            UserDto userDto = new UserDto
            {
                FirstName = userViewModel.FirstName,
                LastName = userViewModel.LastName
            };

            CreateUserInteractor interactor = new CreateUserInteractor(presenter, repository, logger, userDto);

            interactor.Execute();

            if (presenter.ViewModel is UsersViewModel)
                return View(presenter.ViewModel);
            else //(presenter.ViewModel is ErrorOccuranceViewModel)
                return View("ErrorOccurance", presenter.ViewModel);
            //else return some global error view
        }
    }
}