﻿using System;
using System.Collections.Generic;
using Configurator1.Core.Entities;
using Configurator1.Core.Abstract;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Configurator1.Persistance
{
    public class ExceptionThrowingUserRepository : IRepository<User>
    {
        public void Add(User item)
        {
            throw new Exception();
        }

        public void Delete(int id)
        {
            throw new Exception();
        }

        public IEnumerable<User> Get(Expression<Func<User, bool>> condition)
        {
            throw new Exception();
        }

        public void Update(User item)
        {
            throw new Exception();
        }
    }
}
