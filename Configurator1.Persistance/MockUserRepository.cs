﻿using System;
using System.Collections;
using Configurator1.Core.Entities;
using Configurator1.Core.Abstract;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Configurator1.Persistance
{
    public class MockUserRepository : IRepository<User>
    {
        public void Add(User item)
        {
            //Do nothing
            //throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> Get(Expression<Func<User, bool>> condition)
        {
            return new User[]
            {
                new User
                {
                    Id = 1,
                    FirstName = "Marcin",
                    LastName = "Pieprzak"
                },

                new User
                {
                    Id = 2,
                    FirstName = "Jan",
                    LastName = "Kowalski"
                },

                new User
                {
                    Id = 3,
                    FirstName = "Chuck",
                    LastName = "Norris"
                }
            };
        }

        public void Update(User item)
        {
            throw new NotImplementedException();
        }
    }
}
