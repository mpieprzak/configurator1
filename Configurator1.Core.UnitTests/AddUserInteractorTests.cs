using NUnit.Framework;
using Moq;
using Configurator1.Core.Interactors;
using Configurator1.Core.Dtos;
using Configurator1.Core.Abstract;
using Configurator1.Core.Entities;
using System.Linq;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Execute_WhenCalled_ShouldCallRepositoryAddUser()
        {
            var fakePresenter = new Mock<IPresenter<UsersDto>>();
            var fakeRepository = new Mock<IRepository<User>>();
            var fakeLogger = new Mock<ILogger>();
            var fakeUserDto = new Mock<UserDto>();

            CreateUserInteractor addUserInteractor = new CreateUserInteractor(
                fakePresenter.Object, fakeRepository.Object, fakeLogger.Object, fakeUserDto.Object);

            addUserInteractor.Execute();

            fakeRepository.Verify(x => x.Add(It.IsAny<User>()), Times.Once);
        }

        [Test]
        public void Execute_WhenCalled_ShouldCallPresenterShow()
        {
            var fakePresenter = new Mock<IPresenter<UsersDto>>();
            var fakeRepository = new Mock<IRepository<User>>();
            var fakeLogger = new Mock<ILogger>();
            var fakeUserDto = new Mock<UserDto>();

            CreateUserInteractor addUserInteractor = new CreateUserInteractor(
                fakePresenter.Object, fakeRepository.Object, fakeLogger.Object, fakeUserDto.Object);

            addUserInteractor.Execute();

            fakePresenter.Verify(x => x.Show(It.IsAny<UsersDto>()), Times.Once);
        }

        [Test]
        public void Execute_WhenCalled_ShouldCallLogInfo()
        {
            var fakePresenter = new Mock<IPresenter<UsersDto>>();
            var fakeRepository = new Mock<IRepository<User>>();
            var fakeLogger = new Mock<ILogger>();
            var fakeUserDto = new Mock<UserDto>();

            CreateUserInteractor addUserInteractor = new CreateUserInteractor(
                fakePresenter.Object, fakeRepository.Object, fakeLogger.Object, fakeUserDto.Object);

            addUserInteractor.Execute();

            fakeLogger.Verify(x => x.LogInfo(It.IsAny<string>()), Times.Once);
        }

        [Test]
        public void Execute_WhenCalled_AA()
        {
            var fakePresenter = new Mock<IPresenter<UsersDto>>();
            var fakeRepository = new Mock<IRepository<User>>();
            var fakeLogger = new Mock<ILogger>();
            var fakeUserDto = new Mock<UserDto>();

            CreateUserInteractor addUserInteractor = new CreateUserInteractor(
                fakePresenter.Object, fakeRepository.Object, fakeLogger.Object, fakeUserDto.Object);

            addUserInteractor.Execute();

            fakePresenter.Verify(x => x.Show(It.Is<UsersDto>(y => true)));
        }

    }
}