﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Dtos
{
    public class UserDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
