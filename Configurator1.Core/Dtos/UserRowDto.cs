﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Dtos
{
    public class UserRowDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
