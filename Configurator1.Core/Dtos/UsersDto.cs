﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Dtos
{
    public class UsersDto
    {
        public IEnumerable<UserRowDto> UserRows { get; set; }

        public int? SelectedUserId { get; set; }
    }
}
