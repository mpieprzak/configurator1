﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;
using Configurator1.Core.Dtos;
using Configurator1.Core.Entities;
using System.Linq;

namespace Configurator1.Core.Interactors
{
    public class GetUsersInteractor : ICommand
    {
        IPresenter<UsersDto> presenter;

        IRepository<User> repository;

        ILogger logger;

        public GetUsersInteractor(IPresenter<UsersDto> presenter, IRepository<User> repository, ILogger logger)
        {
            this.presenter = presenter;
            this.repository = repository;
            this.logger = logger;
        }

        public bool CanExecute()
        {
            return true;
        }

        public void Execute()
        {
            try
            {
                IEnumerable<User> users = repository.Get(x => true);

                UsersDto usersDto = ConvertToDto(users);

                usersDto.SelectedUserId = 1;

                presenter.Show(usersDto);
            }
            catch (Exception e)
            {
                logger.LogError($"Podczas dodawania użytkownika wystąpił błąd: {e.Message}\r\n Opis błędu: {e.StackTrace}");

                presenter.ShowError(e.Message);
            }

        }

        UsersDto ConvertToDto(IEnumerable<User> users)
        {
            return new UsersDto
            {
                UserRows = users
                    .Select(x => new UserRowDto
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName
                    })
            };
        }
    }
}
