﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;
using Configurator1.Core.Dtos;
using Configurator1.Core.Entities;
using System.Linq;

namespace Configurator1.Core.Interactors
{
    public class CreateUserInteractor : ICommand
    {
        IPresenter<UsersDto> presenter;

        IRepository<User> repository;

        ILogger logger;

        UserDto user;

        public CreateUserInteractor(IPresenter<UsersDto> presenter, IRepository<User> repository, ILogger logger, UserDto user)
        {
            this.presenter = presenter;
            this.repository = repository;
            this.logger = logger;
            this.user = user;
        }

        public bool CanExecute()
        {
            return true;
        }

        public void Execute()
        {
            User userEntity = ConvertToEntity(user);

            try
            {
                repository.Add(userEntity);

                IEnumerable<User> users = repository.Get(x => true);

                logger.LogInfo("Dodano użytkownika");

                UsersDto usersDto = ConvertToDto(users);

                presenter.Show(usersDto);
            }
            catch (Exception e)
            {
                logger.LogError($"Podczas dodawania użytkownika wystąpił błąd: {e.Message}");

                presenter.ShowError("sdf");
            }
        }

        User ConvertToEntity(UserDto userDto)
        {
            return new User
            {
                FirstName = userDto.FirstName,
                LastName = userDto.LastName
            };
        }

        UsersDto ConvertToDto(IEnumerable<User> users)
        {
            return new UsersDto
            {
                UserRows = users
                    .Select(x => new UserRowDto
                    {
                        Id = x.Id,
                        FirstName = x.FirstName,
                        LastName = x.LastName
                    })
            };
        }
    }
}
