﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;
using Configurator1.Core.Dtos;
using Configurator1.Core.Entities;

namespace Configurator1.Core.Interactors
{
    public abstract class CreateInteractor<TDto, TEntity> where TEntity : EntityBase
    {
        IPresenter<TDto> presenter;

        IRepository<TEntity> repository;

        ILogger logger;

        TDto itemDto;

        public CreateInteractor(IPresenter<TDto> presenter, IRepository<TEntity> repository, ILogger logger, TDto itemDto)
        {
            this.presenter = presenter;
            this.repository = repository;
            this.logger = logger;
            this.itemDto = itemDto;
        }

        public virtual bool CanExecute()
        {
            return true;
        }

        public void Execute()
        {
            TEntity entity = Convert(itemDto);

            try
            {
                repository.Add(entity);

                logger.LogInfo("Dodano użytkownika");

                IEnumerable<TEntity> entities = repository.Get(x => true);

                List<TDto> dtos = new List<TDto>();

                foreach (TEntity entity1 in entities)
                {
                    dtos.Add(Convert(entity1));
                }

                //TDto usersDto = Convert(entities);

                //presenter.Show(usersDto);
            }
            catch (Exception e)
            {
                logger.LogError($"Podczas dodawania użytkownika wystąpił błąd: {e.Message}");

                presenter.ShowError("sdf");
            }
        }



        public abstract TEntity Convert(TDto dto);

        public abstract TDto Convert(TEntity entity);
    }
}
