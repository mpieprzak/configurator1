﻿using System;
using System.Collections.Generic;
using System.Text;
using Configurator1.Core.Abstract;

namespace Configurator1.Core.Entities
{
    public class User : EntityBase
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
