﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Abstract
{
    public interface IPresenter<T>
    {
        void Show(T dto);
        void ShowError(string errorMsg);
    }
}
