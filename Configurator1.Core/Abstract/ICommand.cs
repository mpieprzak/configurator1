﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Abstract
{
    public interface ICommand
    {
        bool CanExecute();

        void Execute();
    }
}
