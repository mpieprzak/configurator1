﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Configurator1.Core.Abstract
{
    public interface ILogger
    {
        void LogInfo(string msg);
        void LogWarning(string msg);
        void LogError(string msg);
    }
}
