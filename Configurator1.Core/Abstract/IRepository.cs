﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using Configurator1.Core.Abstract;

namespace Configurator1.Core.Abstract
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> condition);
        void Add(TEntity item);
        void Update(TEntity item);
        void Delete(int id);
    }
}
